**Pig Society** est un collectif jouant de la musique traditionnelle nord-américaine : **oldtime** et **bluegrass**, occasionnellement un morceau québécois ou une chanson française.

Au programme : voix, violon, banjo, guitare, contrebasse, mandoline… ; en trio, quatuor, quintette, voire plus !
