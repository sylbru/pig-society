## Crédits

Conception et développement : Sylvain Brunerie  
Illustrations : Julia Zech

Ce site a été réalisé en Elm avec [elm-pages](https://elm-pages.com/).

## Mentions légales

Responsable légal : Sylvain Brunerie, 14 rue Fernel, 60600 Clermont, FRANCE

Hébergement : [Netlify](https://www.netlify.com/)

Aucune donnée personnelle n’est collectée.

Aucun cookie n’est déposé sur votre appareil.
