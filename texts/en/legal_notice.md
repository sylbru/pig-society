## Credits

Web design and development: Sylvain Brunerie  
Illustrations: Julia Zech

This website was made in Elm with [elm-pages](https://elm-pages.com/).

## Legal notice

Editor: Sylvain Brunerie, 14 rue Fernel, 60600 Clermont, FRANCE

Hosting: [Netlify](https://www.netlify.com/)

No personal data is collected.

No cookies are used or stored in your device.
