**Pig Society** is a collective from France playing traditional American music: **oldtime** and **bluegrass**, as well as the occasional Québécois tune or French song.

On the program: vocals, fiddle, banjo, guitar, double bass, mandolin… ; as a trio, a quartet, quintet, or even more!
