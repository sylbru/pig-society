module Helpers exposing (MultilingualText, langFromRoute, langRoutes, menuItems, routeInLang)

import Icons
import Route exposing (Route(..))
import Svg


type alias MultilingualText =
    { fr : String, en : String }


langRoutes : List { lang : String }
langRoutes =
    [ { lang = "en" }
    , { lang = "fr" }
    ]


langFromRoute : Route -> Maybe String
langFromRoute route =
    case route of
        Lang_ { lang } ->
            Just lang

        Lang___Media { lang } ->
            Just lang

        Lang___Recordings { lang } ->
            Just lang

        Lang___Tour { lang } ->
            Just lang

        Lang___Contact { lang } ->
            Just lang

        Lang___LegalNotice { lang } ->
            Just lang

        Links ->
            Nothing

        Index ->
            Nothing


routeInLang : Route -> String -> Route
routeInLang route destLang =
    case route of
        Lang_ _ ->
            Lang_ { lang = destLang }

        Lang___Media _ ->
            Lang___Media { lang = destLang }

        Lang___Recordings _ ->
            Lang___Recordings { lang = destLang }

        Lang___Tour _ ->
            Lang___Tour { lang = destLang }

        Lang___Contact _ ->
            Lang___Contact { lang = destLang }

        Lang___LegalNotice _ ->
            Lang___LegalNotice { lang = destLang }

        Links ->
            route

        Index ->
            route


menuItems : String -> List ( Route, String, Svg.Svg msg )
menuItems lang =
    case lang of
        "fr" ->
            [ ( Route.Lang_ { lang = "fr" }, "Le collectif", Icons.piggies )
            , ( Route.Lang___Tour { lang = "fr" }, "Dates", Icons.tour )
            , ( Route.Lang___Media { lang = "fr" }, "Média", Icons.media )
            , ( Route.Lang___Recordings { lang = "fr" }, "Disques", Icons.recordings )
            , ( Route.Lang___Contact { lang = "fr" }, "Contact", Icons.contact )
            ]

        "en" ->
            [ ( Route.Lang_ { lang = "en" }, "The collective", Icons.piggies )
            , ( Route.Lang___Tour { lang = "en" }, "Tour", Icons.tour )
            , ( Route.Lang___Media { lang = "en" }, "Media", Icons.media )
            , ( Route.Lang___Recordings { lang = "en" }, "Recordings", Icons.recordings )
            , ( Route.Lang___Contact { lang = "en" }, "Contact", Icons.contact )
            ]

        _ ->
            []
