module TourDate exposing (TourDate, decoder, view)

import Date exposing (Date, Language)
import DateEn
import DateFr
import Html exposing (..)
import Html.Attributes exposing (..)
import Time
import Yaml.Decode


type alias TourDate =
    { date : Date
    , until : Maybe Date
    , location : String
    , venue : Maybe String
    , link : Maybe String
    }


decoder : Yaml.Decode.Decoder (Result String TourDate)
decoder =
    let
        resultDateDecoder : Yaml.Decode.Decoder (Result String Date)
        resultDateDecoder =
            Yaml.Decode.map Date.fromIsoString Yaml.Decode.string

        maybeResultToResultMaybe : Maybe (Result e v) -> Result e (Maybe v)
        maybeResultToResultMaybe maybeResult =
            case maybeResult of
                Nothing ->
                    Ok Nothing

                Just result ->
                    case result of
                        Ok ok ->
                            Ok (Just ok)

                        Err err ->
                            Err err

        makeResultTourDate : Result String Date -> Maybe (Result String Date) -> String -> Maybe String -> Maybe String -> Result String TourDate
        makeResultTourDate resultDate maybeResultUntil location optionalVenue optionalLink =
            Result.map2
                (\date until -> TourDate date until location optionalVenue optionalLink)
                resultDate
                (maybeResultToResultMaybe maybeResultUntil)
    in
    Yaml.Decode.map5
        makeResultTourDate
        (Yaml.Decode.field "date" resultDateDecoder)
        (Yaml.Decode.sometimes <| Yaml.Decode.field "until" resultDateDecoder)
        (Yaml.Decode.field "location" Yaml.Decode.string)
        (Yaml.Decode.sometimes <| Yaml.Decode.field "venue" Yaml.Decode.string)
        (Yaml.Decode.sometimes <| Yaml.Decode.field "link" Yaml.Decode.string)


view : String -> TourDate -> Html msg
view locale tourDate =
    li [ class "tour__date", class "tour-date" ]
        [ span
            [ class "tour-date__date" ]
            [ text <| viewDate locale tourDate.date
            , case tourDate.until of
                Just untilDate ->
                    span [ class "tour-date__until" ] [ text <| " –> " ++ viewDate locale untilDate ]

                Nothing ->
                    emptyNode
            ]
        , span [ class "tour-date__location" ] [ text tourDate.location ]
        , case tourDate.venue of
            Just venue ->
                span [ class "tour-date__venue" ] [ text <| ", " ++ venue ]

            Nothing ->
                emptyNode
        , case tourDate.link of
            Just link ->
                a [ href link, target "_blank", class "tour-date__link" ] [ text "En savoir plus" ]

            Nothing ->
                emptyNode
        ]


viewDate : String -> Date -> String
viewDate locale date =
    let
        ( pattern, language ) =
            case locale of
                "fr" ->
                    ( "EEEE ddd MMMM y", DateFr.language )

                _ ->
                    ( "EEEE, ddd MMMM y", DateEn.language )
    in
    Date.formatWithLanguage language pattern date


emptyNode : Html msg
emptyNode =
    text ""
