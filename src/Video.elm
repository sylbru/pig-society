module Video exposing (Video, burningBushHalfPastFour, bowlingGreen, cdf2018Rachel, cdfSession, hebdenBridge, more, muziekPublique, noneButOne, reimsVanTor, toIframe, voixVerte)

import Html exposing (Html)
import Html.Attributes as Attr
import Html.Events as Events


type Video
    = Video String


toIframe : Video -> List Video -> (Video -> msg) -> Html msg
toIframe ((Video videoId) as video) loadedVideos clickedMsg =
    Html.div
        [ Attr.style "background" "#333", Attr.class "video" ]
        [ if List.member video loadedVideos then
            Html.iframe
                [ Attr.class "video"
                , Attr.src (idToEmbed videoId)
                , Attr.title "Video"
                , Attr.attribute "frameborder" "0"
                , Attr.attribute "allow" "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                , Attr.attribute "allowfullscreen" ""
                ]
                []

          else
            Html.a
                [ Attr.class "video"
                , Attr.style "background" "#333"
                , Attr.style "background-image" ("url(" ++ idToThumbnail videoId ++ ")")
                , Attr.style "background-size" "cover"
                , Attr.style "background-position" "center center"
                , Attr.style "cursor" "pointer"
                , Attr.target "_blank"
                , Attr.href (idToLink videoId)
                , Events.onMouseOver (clickedMsg video)
                , Events.onFocus (clickedMsg video)
                ]
                []
        ]


idToLink : String -> String
idToLink videoId =
    "https://www.youtube.com/watch?v=" ++ videoId


idToEmbed : String -> String
idToEmbed videoId =
    "//www.youtube.com/embed/" ++ videoId


idToThumbnail : String -> String
idToThumbnail videoId =
    "https://img.youtube.com/vi/" ++ videoId ++ "/0.jpg"


muziekPublique : Video
muziekPublique =
    Video "RQ0Z9j2qnRo"


noneButOne : Video
noneButOne =
    Video "wPv-cpvz95E"


hebdenBridge : Video
hebdenBridge =
    Video "jS9ub68LLFk"


reimsVanTor : Video
reimsVanTor =
    Video "J7GCsehC9Pc"


cdfSession : Video
cdfSession =
    Video "TZqT4pZEjCo"


bowlingGreen : Video
bowlingGreen =
    Video "0oZCtwXRh8o"


voixVerte : Video
voixVerte =
    Video "Eg5s1jHodzw"


cdf2018Rachel : Video
cdf2018Rachel =
    Video "9ZVMJCezLFM"

burningBushHalfPastFour : Video
burningBushHalfPastFour =
  Video "8IdiYY7LL-8"


more : List Video
more =
    [ hebdenBridge
    , voixVerte
    , cdf2018Rachel
    , Video "8v3Bs_Ecg5k" -- CDF 2018, Indian Eat The Woodchuck
    , Video "_9uhAfXn-Ws" -- Guild Sessions, Be Kind
    , Video "3P2prcti3bk" -- Nancy, Wild Hog In The Woods
    , Video "pVEHNjsYUFM" -- Road To Malvern
    ]


emptyAttribute : Html.Attribute msg
emptyAttribute =
    Attr.classList []
