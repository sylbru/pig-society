module Quote exposing (QuotePosition(..), bordel, bouffe, celtique, pasMal, petiteMusique, plusFort, tempsMort, tout, view, vite, voyage)

import Html exposing (Html)
import Html.Attributes as Attr


type alias Quote =
    { fr : { contents : String, attribution : String }
    , en : { contents : String, attribution : String }
    }


type QuotePosition
    = Default
    | Left
    | Right


quoteContentsToLang : String -> Quote -> String
quoteContentsToLang lang { fr, en } =
    case lang of
        "fr" ->
            "«\u{202F}" ++ fr.contents ++ "\u{202F}»"

        _ ->
            "“" ++ en.contents ++ "”"


quoteAttributionToLang : String -> Quote -> String
quoteAttributionToLang lang { fr, en } =
    case lang of
        "fr" ->
            "— " ++ fr.attribution

        _ ->
            "—" ++ en.attribution


view : String -> QuotePosition -> Quote -> Html msg
view lang quotePosition quote =
    let
        position =
            case quotePosition of
                Left ->
                    [ Attr.class "quote-container--left" ]

                Right ->
                    [ Attr.class "quote-container--right" ]

                Default ->
                    []
    in
    Html.div ([ Attr.class "quote-container" ] ++ position)
        [ Html.figure
            [ Attr.class "quote" ]
            [ Html.blockquote
                [ Attr.class "quote__contents"
                ]
                [ Html.text <| quoteContentsToLang lang quote ]
            , Html.figcaption
                [ Attr.class "quote__attribution"
                ]
                [ Html.text <| quoteAttributionToLang lang quote ]
            ]
        ]


bordel : Quote
bordel =
    { fr = { contents = "C’est un peu le bordel.", attribution = "Alexandre (Charlie et sa bière à deux balles)" }
    , en = { contents = "It’s a bit of a mess.", attribution = "Alexandre (Charlie et sa bière à deux balles)" }
    }


petiteMusique : Quote
petiteMusique =
    { fr = { contents = "Vous savez, il n’y a pas de petite musique.", attribution = "Un classiqueux anonyme" }
    , en = { contents = "I believe all music genres are equal.", attribution = "A classical musician" }
    }


tout : Quote
tout =
    { fr = { contents = "Moi, j’écoute de tout.", attribution = "La plupart des gens" }
    , en = { contents = "I listen to all kinds of music.", attribution = "Most people" }
    }


pasMal : Quote
pasMal =
    { fr = { contents = "Il était pas mal ce morceau, mais je préférais celui d’avant.", attribution = "Arthur, 8\u{00A0}ans" }
    , en = { contents = "That tune was ok, but I preferred the one before.", attribution = "Arthur, 8\u{00A0}years old" }
    }


vite : Quote
vite =
    { fr = { contents = "Comment vous faites pour jouer aussi vite\u{202F}?", attribution = "Lucie, 9\u{00A0}ans" }
    , en = { contents = "How do you play so fast?", attribution = "Lucie, 9\u{00A0}years old" }
    }


voyage : Quote
voyage =
    { fr = { contents = "Une véritable invitation au voyage.", attribution = "Un communicant inspiré" }
    , en = { contents = "Pig Society takes us on a musical journey through Appalachia.", attribution = "An imaginative marketing guy" }
    }


tempsMort : Quote
tempsMort =
    { fr = { contents = "Stop, temps mort\u{202F}! Vous vous croyez où\u{202F}?", attribution = "Organisateur de l’événement, pendant une répétition" }
    , en = { contents = "Oy! Stop it! Where do you think you are?", attribution = "Event organizer, during a rehearsal" }
    }


plusFort : Quote
plusFort =
    { fr = { contents = "Est-ce que vous pouvez chanter un peu plus fort\u{202F}?", attribution = "Un ingénieur du son indélicat" }
    , en = { contents = "Can you sing a bit louder?", attribution = "An indelicate soundguy" }
    }


celtique : Quote
celtique =
    { fr = { contents = "J’adore la musique celtique\u{202F}! Vous jouez Les Lacs du Connemara\u{202F}?", attribution = "Quelqu’un qui n’a pas suivi" }
    , en = { contents = "I love Celtic music! Can you play Wagon Wheel?", attribution = "Someone who didn’t quite follow" }
    }


bouffe : Quote
bouffe =
    { fr = { contents = "Ça envoie du pâté, même quand ils jouent des saucissons.", attribution = "Rafi F. & Darius L." }
    , en = { contents = "They love chestnuts, but at least they bring home the bacon.", attribution = "Rafi F. & Darius L." }
    }
