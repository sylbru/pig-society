module DateFr exposing (language)

import Date exposing (Language, weekday)
import Time exposing (Month(..), Weekday(..))


language : Language
language =
    { monthName = monthName
    , monthNameShort = monthNameShort
    , weekdayName = weekdayName
    , weekdayNameShort = weekdayNameShort
    , dayWithSuffix = dayWithSuffix
    }


monthName : Month -> String
monthName month =
    case month of
        Jan ->
            "janvier"

        Feb ->
            "février"

        Mar ->
            "mars"

        Apr ->
            "avril"

        May ->
            "mai"

        Jun ->
            "juin"

        Jul ->
            "juillet"

        Aug ->
            "août"

        Sep ->
            "septembre"

        Oct ->
            "octobre"

        Nov ->
            "novembre"

        Dec ->
            "décembre"


monthNameShort : Month -> String
monthNameShort month =
    case month of
        Jan ->
            "jan"

        Feb ->
            "fév"

        Mar ->
            "mar"

        Apr ->
            "avr"

        May ->
            "mai"

        Jun ->
            "juin"

        Jul ->
            "juil"

        Aug ->
            "aoû"

        Sep ->
            "sep"

        Oct ->
            "oct"

        Nov ->
            "nov"

        Dec ->
            "déc"


weekdayName : Weekday -> String
weekdayName weekday =
    case weekday of
        Mon ->
            "lundi"

        Tue ->
            "mardi"

        Wed ->
            "mercredi"

        Thu ->
            "jeudi"

        Fri ->
            "vendredi"

        Sat ->
            "samedi"

        Sun ->
            "dimanche"


weekdayNameShort : Weekday -> String
weekdayNameShort weekday =
    case weekday of
        Mon ->
            "lun"

        Tue ->
            "mar"

        Wed ->
            "mer"

        Thu ->
            "jeu"

        Fri ->
            "ven"

        Sat ->
            "sam"

        Sun ->
            "dim"


dayWithSuffix : Int -> String
dayWithSuffix day =
    case day of
        1 ->
            "1er"

        other ->
            String.fromInt other
