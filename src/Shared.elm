module Shared exposing (Data, Model, Msg(..), SharedMsg(..), siteName, template, titleWith)

import Browser.Navigation
import DataSource
import Helpers
import Html exposing (Html)
import Html.Attributes as Attr exposing (class)
import Pages.Flags
import Pages.PageUrl exposing (PageUrl)
import Path exposing (Path)
import Route exposing (Route)
import SharedTemplate exposing (SharedTemplate)
import View exposing (View)


template : SharedTemplate Msg Model Data msg
template =
    { init = init
    , update = update
    , view = view
    , data = data
    , subscriptions = subscriptions
    , onPageChange = Just OnPageChange
    }


type Msg
    = OnPageChange
        { path : Path
        , query : Maybe String
        , fragment : Maybe String
        }
    | SharedMsg SharedMsg


type alias Data =
    ()


type SharedMsg
    = NoOp


type alias Model =
    { showMobileMenu : Bool
    }


init :
    Maybe Browser.Navigation.Key
    -> Pages.Flags.Flags
    ->
        Maybe
            { path :
                { path : Path
                , query : Maybe String
                , fragment : Maybe String
                }
            , metadata : route
            , pageUrl : Maybe PageUrl
            }
    -> ( Model, Cmd Msg )
init navigationKey flags maybePagePath =
    ( { showMobileMenu = False }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnPageChange _ ->
            ( { model | showMobileMenu = False }, Cmd.none )

        SharedMsg globalMsg ->
            ( model, Cmd.none )


subscriptions : Path -> Model -> Sub Msg
subscriptions _ _ =
    Sub.none


data : DataSource.DataSource Data
data =
    DataSource.succeed ()


view :
    Data
    ->
        { path : Path
        , route : Maybe Route
        }
    -> Model
    -> (Msg -> msg)
    -> View msg
    -> { body : Html msg, title : String }
view sharedData page model toMsg pageView =
    { body = body page.route pageView.body
    , title = pageView.title
    }


siteName : String
siteName =
    "Pig Society"


titleWith : String -> String
titleWith title =
    title ++ " | " ++ siteName


body : Maybe Route -> List (Html msg) -> Html msg
body maybeRoute pageBody =
    let
        home =
            maybeRoute
                |> Maybe.andThen Helpers.langFromRoute
                |> Maybe.map (\slug -> Route.Lang_ { lang = slug })
                |> Maybe.withDefault Route.Index
    in
    Html.div [ Attr.class "container" ]
        [ optionalLanguageSwitcher maybeRoute
        , Html.h1
            [ Attr.class "pig-society-title" ]
            [ Route.link
                home
                [ Attr.style "display" "inline-block"
                , Attr.style "transform" "scaleX(120%)"
                , Attr.style "padding" "0 1em"
                ]
                [ Html.text "Pig", Html.span [ Attr.style "font-size" "0.5em" ] [ Html.text " " ], Html.text "Society" ]
            ]
        , optionalMenu maybeRoute
        , Html.main_ [ Attr.class "main-content" ] pageBody
        , optionalFooter maybeRoute
        ]


optionalMenu : Maybe Route -> Html msg
optionalMenu maybeRoute =
    case maybeRoute of
        Just route ->
            menu route

        Nothing ->
            Html.text ""


menu : Route -> Html msg
menu currentRoute =
    let
        link ( route, label, icon ) =
            let
                optionalCurrent =
                    if route == currentRoute then
                        [ Attr.class "menu-item--current" ]

                    else
                        []
            in
            Route.link
                route
                (Attr.class "menu-item" :: optionalCurrent)
                [ icon, Html.text label ]
    in
    case Helpers.langFromRoute currentRoute of
        Just lang ->
            Html.nav [ Attr.class "menu" ]
                (List.map link (Helpers.menuItems lang))

        _ ->
            Html.text ""


optionalLanguageSwitcher : Maybe Route -> Html msg
optionalLanguageSwitcher maybeRoute =
    case maybeRoute of
        Just route ->
            languageSwitcher route

        Nothing ->
            Html.text ""


languageSwitcher : Route -> Html msg
languageSwitcher route =
    let
        link slug label =
            Route.link
                (Helpers.routeInLang route slug)
                [ Attr.hreflang slug ]
                [ Html.text <| label ]
    in
    Html.nav
        [ Attr.class "language-switcher" ]
        [ case Helpers.langFromRoute route of
            Just "fr" ->
                link "en" "(English?)"

            Just "en" ->
                link "fr" "(Français\u{202F}?)"

            _ ->
                Html.text ""
        ]


optionalFooter : Maybe Route -> Html msg
optionalFooter maybeRoute =
    case maybeRoute of
        Just route ->
            footer (Helpers.langFromRoute route |> Maybe.withDefault "en")

        Nothing ->
            Html.text ""


footer : String -> Html msg
footer lang =
    let
        linkText =
            case lang of
                "fr" ->
                    "Crédits & Mentions légales"

                _ ->
                    "Credits & Legal notice"
    in
    Html.div [ class "footer" ]
        [ Route.link (Route.Lang___LegalNotice { lang = lang }) [] [ Html.text linkText ]
        ]
