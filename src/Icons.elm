module Icons exposing (contact, media, piggies, recordings, tour)

import Svg exposing (..)
import Svg.Attributes exposing (..)


recordings : Svg msg
recordings =
    svg
        baseAttributes
        [ circle
            [ cx "10"
            , cy "10"
            , r "6"
            , strokeWidth "6"
            , fill "none"
            ]
            []
        ]


media : Svg msg
media =
    let
        viewBoxSize =
            20

        filmWidth =
            16

        filmHeight =
            14

        holeSize =
            filmHeight / 4

        innerFilmWidth =
            filmWidth - (2 * holeSize)

        filmY =
            (viewBoxSize - filmHeight) / 2

        innerFilmX =
            (viewBoxSize - innerFilmWidth) / 2

        hole : Int -> Bool -> Svg msg
        hole index rightSide =
            rect
                [ fill "none"
                , strokeWidth <| String.fromFloat (holeSize / 3)
                , width <| String.fromFloat holeSize
                , height <| String.fromFloat holeSize
                , x <|
                    String.fromFloat
                        (if rightSide then
                            innerFilmX + innerFilmWidth

                         else
                            innerFilmX - holeSize
                        )
                , y <| String.fromFloat (filmY + toFloat index * holeSize)
                ]
                []
    in
    svg
        baseAttributes
        [ rect
            [ x <| String.fromFloat innerFilmX
            , y <| String.fromFloat filmY
            , strokeWidth "1.2"
            , fill "currentColor"
            , width <| String.fromFloat innerFilmWidth
            , height <| String.fromFloat filmHeight
            ]
            []
        , hole 0 False
        , hole 0 True
        , hole 1 False
        , hole 1 True
        , hole 2 False
        , hole 2 True
        , hole 3 False
        , hole 3 True
        ]


tour : Svg msg
tour =
    let
        tieWidth =
            2

        viewBoxSize =
            20

        tie xOffset =
            rect
                [ x <| String.fromFloat (viewBoxSize / 2 - (tieWidth / 2) + xOffset)
                , y "0"
                , rx "1.5"
                , width <| String.fromFloat tieWidth
                , height "5"
                , stroke "none"
                , fill "currentColor"
                ]
                []
    in
    svg
        baseAttributes
        [ rect
            [ x "2"
            , y "2"
            , width "16"
            , height "16"
            , rx "3"
            , fill "none"
            ]
            []
        , text_
            [ x "7"
            , y "15"
            , fontFamily "monospace"
            , fontSize "10"
            , fill "currentColor"
            ]
            [ text "8" ]
        , tie -4
        , tie 0
        , tie 4
        ]


piggies : Svg msg
piggies =
    svg
        [ width "35"
        , height "25"
        , viewBox "0 0 30 20"
        , stroke "currentColor"
        ]
        [ defs
            []
            [ g [ id "piggy" ]
                [ Svg.path
                    [ d "M 0,15 L 1,5 L 6,5 L 7,15 z", fill "currentColor" ]
                    []
                , ellipse [ cx "3.5", cy "4", rx "3.5", ry "3.5", fill "currentColor" ] []
                ]
            ]
        , use [ xlinkHref "#piggy", transform "translate(1 2)" ] []
        , use [ xlinkHref "#piggy", transform "translate(10 2)" ] []
        , use [ xlinkHref "#piggy", transform "translate(19 2)" ] []

        --[ Svg.clipPath [ id "eye" ] [] ]
        -- Svg.Attributes.clipPath "url(#eyirtualDome)"
        ]


contact : Svg msg
contact =
    svg
        (strokeWidth "1.5" :: baseAttributes)
        [ rect [ x "1", y "3", width "18", height "13", fill "none" ] []
        , Svg.path [ d "M1,3 l9,7 l9,-7", fill "none" ] []
        ]


baseAttributes : List (Svg.Attribute msg)
baseAttributes =
    [ width "25"
    , height "25"
    , viewBox "0 0 20 20"
    , stroke "currentColor"
    ]
