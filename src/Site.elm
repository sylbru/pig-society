module Site exposing (config)

import Color
import DataSource
import Head
import MimeType
import Pages.Manifest as Manifest exposing (withThemeColor)
import Pages.Url exposing (Url)
import Path
import Route
import SiteConfig exposing (SiteConfig)


type alias Data =
    ()


config : SiteConfig Data
config =
    { data = data
    , canonicalUrl = "https://pigsociety.eu"
    , manifest = manifest
    , head = head
    }


data : DataSource.DataSource Data
data =
    DataSource.succeed ()


head : Data -> List Head.Tag
head static =
    [ Head.appleTouchIcon (Just 180) (Pages.Url.fromPath <| Path.fromString "/images/icon-192.png")

    -- These don’t work with the dev server because the canonical isn’t the same
    , Head.icon [ ( 192, 192 ) ] MimeType.Png (Pages.Url.fromPath <| Path.fromString "/images/icon-192.png")
    , Head.icon [ ( 32, 32 ), ( 16, 16 ) ] (MimeType.OtherImage "x-icon") (Pages.Url.fromPath <| Path.fromString "/favicon.ico")
    ]


manifest : Data -> Manifest.Config
manifest static =
    Manifest.init
        { name = "Pig Society"
        , description = "Un groupe/collectif de France (principalement) jouant de la musique traditionnelle américaine : oldtime et bluegrass. / A band/collective from France (mostly) playing American old-time music and bluegrass."
        , startUrl = Route.Index |> Route.toPath
        , icons =
            [ { src = Pages.Url.fromPath <| Path.fromString "/images/icon-192.png", sizes = [ ( 192, 192 ) ], mimeType = Just MimeType.Png, purposes = [] }
            , { src = Pages.Url.fromPath <| Path.fromString "/images/icon-512.png", sizes = [ ( 512, 512 ) ], mimeType = Just MimeType.Png, purposes = [] }
            ]
        }
        |> withThemeColor (Color.rgb255 255 200 170)
