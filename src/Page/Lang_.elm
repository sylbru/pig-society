module Page.Lang_ exposing (Data, Model, Msg, page)

import Browser.Navigation
import DataSource exposing (DataSource)
import DataSource.File
import Head
import Head.Seo as Seo
import Helpers exposing (MultilingualText)
import Html exposing (Html)
import Html.Attributes as Attr
import LanguageTag
import LanguageTag.Language
import Markdown
import Page exposing (Page, PageWithState, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Path
import Quote
import Route
import Shared
import Video exposing (Video)
import View exposing (View)



----------------------------
-- Index / The collective --
----------------------------


type alias Model =
    { loadedVideos : List Video }


type Msg
    = ClickedVideo Video


type alias RouteParams =
    { lang : String }


page : PageWithState RouteParams Data Model Msg
page =
    Page.prerender
        { head = head
        , routes = routes
        , data = data
        }
        |> Page.buildWithLocalState { view = view, init = init, update = update, subscriptions = subscriptions }


routes : DataSource (List RouteParams)
routes =
    DataSource.succeed Helpers.langRoutes


data : RouteParams -> DataSource Data
data routeParams =
    DataSource.map2
        (\a b -> { intro = a, more = b })
        (multilingualTextDataSourceFromPaths "texts/fr/index.md" "texts/en/index.md")
        (multilingualTextDataSourceFromPaths "texts/fr/index_more.md" "texts/en/index_more.md")


multilingualTextDataSourceFromPaths : String -> String -> DataSource MultilingualText
multilingualTextDataSourceFromPaths pathFr pathEn =
    DataSource.map2 (\fr en -> { fr = fr, en = en })
        (DataSource.File.bodyWithoutFrontmatter pathFr)
        (DataSource.File.bodyWithoutFrontmatter pathEn)


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    let
        language =
            (if static.routeParams.lang == "fr" then
                LanguageTag.Language.fr

             else
                LanguageTag.Language.en
            )
                |> LanguageTag.build LanguageTag.emptySubtags
                |> Head.rootLanguage

        texts =
            if static.routeParams.lang == "fr" then
                { description = "Pig Society est un collectif oldtime/bluegrass de France (et d’ailleurs)."
                , alt = "Pig Society - Celebration Days Live Session"
                , title = "Pig Society - Le collectif"
                }

            else
                { description = "Pig Society is an oldtime/bluegrass collective from France (and more)."
                , alt = "Pig Society - Celebration Days Live Session"
                , title = "Pig Society - The collective"
                }

        seo =
            Seo.summary
                { canonicalUrlOverride = Nothing
                , siteName = "Pig Society"
                , image =
                    { url = Pages.Url.fromPath <| Path.fromString "/images/pigsociety-celebrationdays-livesession.jpg"
                    , alt = texts.alt
                    , dimensions = Just { width = 1200, height = 641 }
                    , mimeType = Just "image/jpeg"
                    }
                , description = texts.description
                , locale = Just static.routeParams.lang
                , title = texts.title
                }
                |> Seo.website
    in
    language :: seo


type alias Data =
    { intro : MultilingualText
    , more : MultilingualText
    }


init : Maybe PageUrl -> Shared.Model -> StaticPayload Data RouteParams -> ( Model, Cmd Msg )
init _ _ _ =
    ( { loadedVideos = []
      }
    , Cmd.none
    )


update : PageUrl -> Maybe Browser.Navigation.Key -> Shared.Model -> StaticPayload Data RouteParams -> Msg -> Model -> ( Model, Cmd Msg )
update _ _ _ _ msg model =
    case msg of
        ClickedVideo video ->
            ( { model
                | loadedVideos =
                    if not <| List.member video model.loadedVideos then
                        video :: model.loadedVideos

                    else
                        model.loadedVideos
              }
            , Cmd.none
            )


subscriptions : Maybe PageUrl -> RouteParams -> Path.Path -> Model -> Sub Msg
subscriptions _ _ _ _ =
    Sub.none


view :
    Maybe PageUrl
    -> Shared.Model
    -> Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel localModel static =
    let
        lang =
            static.routeParams.lang

        toLang : String -> MultilingualText -> String
        toLang someLang someData =
            if lang == "fr" then
                someData.fr

            else
                someData.en
    in
    { title = Shared.siteName
    , body =
        List.concat
            [ [ image lang ]
            , Markdown.toHtml Nothing (static.data.intro |> toLang lang)
            , [ viewVideo localModel.loadedVideos ]
            , Markdown.toHtml Nothing (static.data.more |> toLang lang)
            , [ Quote.view lang Quote.Right Quote.bordel ]
            ]
    }


image : String -> Html Msg
image lang =
    let
        altText =
            if lang == "fr" then
                "Le Cochon Ballon transportant violon, banjo, guitare et contrebasse"

            else
                "The Piggy Balloon carrying a fiddle, banjo, guitar and double bass"
    in
    Html.img
        [ Attr.src "/pigsociety-balloon-web.png"
        , Attr.style "height" "auto"
        , Attr.style "max-width" "10em"
        , Attr.style "margin-top" "-.5em"
        , Attr.style "margin-bottom" "-1em"
        , Attr.alt altText
        ]
        []


viewVideo : List Video -> Html Msg
viewVideo loadedVideos =
    Video.toIframe Video.burningBushHalfPastFour loadedVideos ClickedVideo
