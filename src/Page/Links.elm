module Page.Links exposing (Data, Model, Msg, page)

import DataSource exposing (DataSource)
import Head
import Head.Seo as Seo
import Helpers
import Html exposing (Html)
import Html.Attributes
import LanguageTag
import LanguageTag.Language
import Page exposing (Page, PageWithState, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Path
import Route
import Shared
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    {}


page : Page RouteParams Data
page =
    Page.single
        { head = head
        , data = data
        }
        |> Page.buildNoState { view = view }


routes : DataSource ()
routes =
    DataSource.succeed ()


data : DataSource Data
data =
    DataSource.succeed <|
        [ { url = "https://pig-society.bandcamp.com/album/vol-3-pig-in-the-north"
          , title = "Pig Society Vol. 3 – Pig in the North (Bandcamp)"
          }
        , { url = "https://www.youtube.com/watch?v=wPv-cpvz95E"
          , title = "Video: None But One (YouTube)"
          }
        , { url = "https://www.youtube.com/watch?v=NQFjb5YI7pg"
          , title = "Video: Moon Over Montana (YouTube)"
          }
        , { url = "https://pigsociety.eu"
          , title = "Pig Society Website"
          }
        ]


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    let
        language =
            LanguageTag.Language.en
                |> LanguageTag.build LanguageTag.emptySubtags
                |> Head.rootLanguage

        texts =
            { description = "Pig Society: links"
            , alt = ""
            , title = "Links"
            }

        seo =
            Seo.summary
                { canonicalUrlOverride = Nothing
                , siteName = Shared.siteName
                , image =
                    { url = Pages.Url.fromPath <| Path.fromString "/images/pigsociety-celebrationdays-livesession.jpg"
                    , alt = "Pig Society - Celebration Days Live Session"
                    , dimensions = Just { width = 1200, height = 641 }
                    , mimeType = Just "image/jpeg"
                    }
                , description = texts.description
                , locale = Just "en"
                , title = texts.title
                }
                |> Seo.website
    in
    language :: seo


type alias Data =
    List Link


type alias Link =
    { url : String
    , title : String
    }


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    let
        viewLink : Link -> Html Msg
        viewLink { url, title } =
            Html.li []
                [ Html.a
                    [ Html.Attributes.class "links__link"
                    , Html.Attributes.href url
                    , Html.Attributes.target "_blank"
                    ]
                    [ Html.text title ]
                ]
    in
    { title = "Pig Society"
    , body =
        [ Html.ul [ Html.Attributes.class "links" ]
            (List.map viewLink static.data)
        ]
    }
