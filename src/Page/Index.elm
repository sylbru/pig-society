module Page.Index exposing (Data, Model, Msg, page)

import DataSource exposing (DataSource)
import Head
import Head.Seo as Seo
import Html
import Page exposing (Page, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Path
import Route
import Shared
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    {}


page : Page RouteParams Data
page =
    Page.single
        { head = head
        , data = data
        }
        |> Page.buildNoState { view = view }


data : DataSource Data
data =
    DataSource.succeed ()


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = "Pig Society"
        , image =
            { url = Pages.Url.fromPath <| Path.fromString "/images/pigsociety-celebrationdays-livesession.jpg"
            , alt = "Pig Society - Celebration Days Live Session"
            , dimensions = Just { width = 1200, height = 641 }
            , mimeType = Just "image/jpeg"
            }
        , description = "Pig Society"
        , locale = Nothing
        , title = "Pig Society"
        }
        |> Seo.website


type alias Data =
    ()


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    { title = "Pig Society"
    , body =
        [ Html.p []
            [ Route.link (Route.Lang_ { lang = "fr" })
                []
                [ Html.text <| String.toUpper "En français" ]
            , Html.text " / "
            , Route.link (Route.Lang_ { lang = "en" })
                []
                [ Html.text <| String.toUpper "In English" ]
            ]
        ]
    }
