module Page.Lang_.LegalNotice exposing (Data, Model, Msg, page)

import DataSource exposing (DataSource)
import DataSource.File
import Head
import Head.Seo as Seo
import Helpers exposing (MultilingualText)
import Html exposing (..)
import Html.Attributes exposing (..)
import Markdown
import Page exposing (Page, PageWithState, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Shared
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    { lang : String }


page : Page RouteParams Data
page =
    Page.prerender
        { head = head
        , routes = routes
        , data = data
        }
        |> Page.buildNoState { view = view }


routes : DataSource (List RouteParams)
routes =
    DataSource.succeed Helpers.langRoutes


data : RouteParams -> DataSource Data
data routeParams =
    multilingualTextDataSourceFromPaths "texts/fr/legal_notice.md" "texts/en/legal_notice.md"


multilingualTextDataSourceFromPaths : String -> String -> DataSource MultilingualText
multilingualTextDataSourceFromPaths pathFr pathEn =
    DataSource.map2 (\fr en -> { fr = fr, en = en })
        (DataSource.File.bodyWithoutFrontmatter pathFr)
        (DataSource.File.bodyWithoutFrontmatter pathEn)


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head _ =
    []


type alias Data =
    MultilingualText


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    let
        lang =
            static.routeParams.lang

        toLang : String -> MultilingualText -> String
        toLang someLang someData =
            if lang == "fr" then
                someData.fr

            else
                someData.en
    in
    { title = "Crédits et mentions légales"
    , body =
        Markdown.toHtml Nothing (static.data |> toLang lang)
    }
