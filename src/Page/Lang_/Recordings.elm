module Page.Lang_.Recordings exposing (Data, Model, Msg, page)

import DataSource exposing (DataSource)
import Head
import Head.Seo as Seo
import Helpers
import Html
import Html.Attributes exposing (class)
import LanguageTag
import LanguageTag.Language
import Page exposing (Page, PageWithState, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Quote
import Shared
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    { lang : String }


page : Page RouteParams Data
page =
    Page.prerender
        { head = head
        , routes = routes
        , data = data
        }
        |> Page.buildNoState { view = view }


routes : DataSource (List RouteParams)
routes =
    DataSource.succeed Helpers.langRoutes


data : RouteParams -> DataSource Data
data routeParams =
    DataSource.succeed
        [ { title = "Vol. 3 – Pig in the North"
          , cover = "https://f4.bcbits.com/img/a3333110886_16.jpg"
          , link = "https://pig-society.bandcamp.com/album/vol-3-pig-in-the-north"
          , bandcampAlbumId = 3999104360
          , tracks =
                [ "In the Pines"
                , "Salvation Has Been Brought Down"
                , "Rosin on the Gourd"
                , "Sweet Bunch of Daisies"
                , "Fishin’ Blues"
                , "None But One"
                , "Ways of the World"
                , "Valse de la vie"
                , "Moon Over Montana"
                , "Indians Over the Hill"
                , "Sweet Fern"
                , "Chips and Sauce"
                , "Been on the Line"
                ]
          , released = True
          }
        , { title = "Vol. 2 – Full House"
          , cover = "https://f4.bcbits.com/img/a0404468992_16.jpg"
          , link = "https://pig-society.bandcamp.com/album/vol-2-full-house"
          , bandcampAlbumId = 578006207
          , tracks =
                [ "Georgia Horseshoe"
                , "Be Kind to a Man While He's Down"
                , "Indian Ate the Woodchuck"
                , "Shenandoah Valley Waltz"
                , "Laughing Boy / Lost Indian"
                , "On the Rock Where Moses Stood"
                , "Swing And Turn, Jubilee"
                , "Flatwoods"
                , "Your Cheatin' Heart"
                , "Five Miles of Ellum Wood"
                , "La Bergère"
                , "Sleepy Eyed Joe"
                ]
          , released = True
          }
        , { title = "Vol. 1 – Trio"
          , cover = "https://f4.bcbits.com/img/a1922407178_10.jpg"
          , link = "https://pig-society.bandcamp.com/album/vol-1-trio"
          , bandcampAlbumId = 2395936758
          , tracks =
                [ "Le Petit Cheval / Sweet Marie"
                , "Coal Harbor Bend"
                , "Trials, Troubles, Tribulations"
                , "Wild Hog in the Woods"
                , "Beautiful Brown Eyes"
                , "Big Hoedown"
                , "Ménilmontant"
                , "Indian Eat the Woodpecker"
                , "Fall on My Knees"
                ]
          , released = True
          }
        ]


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    let
        language =
            (if static.routeParams.lang == "fr" then
                LanguageTag.Language.fr

             else
                LanguageTag.Language.en
            )
                |> LanguageTag.build LanguageTag.emptySubtags
                |> Head.rootLanguage

        texts =
            if static.routeParams.lang == "fr" then
                { description = "Tous les disques de Pig Society"
                , alt = "Pig Society Vol.2 - Full House"
                , title = "Disques"
                }

            else
                { description = "All of Pig Society recordings"
                , alt = "Pig Society Vol.2 - Full House"
                , title = "Recordings"
                }

        seo =
            Seo.summary
                { canonicalUrlOverride = Nothing
                , siteName = "Pig Society"
                , image =
                    { url = Pages.Url.external "https://f4.bcbits.com/img/a0404468992_16.jpg"
                    , alt = texts.alt
                    , dimensions = Just { width = 700, height = 700 }
                    , mimeType = Just "image/jpeg"
                    }
                , description = texts.description
                , locale = Just static.routeParams.lang
                , title = texts.title
                }
                |> Seo.website
    in
    language :: seo


type alias Recording =
    { title : String
    , cover : String
    , tracks : List String
    , link : String
    , bandcampAlbumId : Int
    , released : Bool
    }


type alias Data =
    List Recording


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    let
        lang =
            static.routeParams.lang
    in
    { title = title lang
    , body =
        [ Quote.view lang Quote.Right Quote.celtique ]
            ++ List.map (viewRecording lang) static.data
            ++ [ Quote.view lang Quote.Left Quote.plusFort ]
    }


viewRecording : String -> Recording -> Html.Html msg
viewRecording lang recording =
    let
        ( listenOrBuyLabel, comingSoonLabel, preorderLabel ) =
            if lang == "fr" then
                ( "Écouter/Acheter"
                , "Prochainement"
                , "Précommander"
                )

            else
                ( "Listen/Buy"
                , "Coming soon"
                , "Preorder"
                )
    in
    Html.section [ class "recording" ]
        (List.concat
            [ [ Html.h2 [ class "recording__title" ] [ Html.text recording.title ]
              , Html.a [ Html.Attributes.href recording.link, Html.Attributes.target "_blank" ]
                    [ Html.img [ class "recording__cover", Html.Attributes.src recording.cover ] [] ]
              , Html.ol [ class "recording__tracks" ] (List.map (Html.text >> List.singleton >> Html.li []) recording.tracks)
              ]
            , if recording.released then
                [ bandcampPlayer recording.bandcampAlbumId recording.link recording.title
                , Html.a [ class "recording__link", Html.Attributes.href recording.link, Html.Attributes.target "_blank" ]
                    [ Html.text ("› " ++ listenOrBuyLabel ++ " ‹") ]
                ]

              else
                [ Html.p [ Html.Attributes.class "coming-soon" ] [ Html.text comingSoonLabel ]
                , Html.br [] []
                , Html.a [ class "recording__link", Html.Attributes.href recording.link, Html.Attributes.target "_blank" ]
                    [ Html.text ("› " ++ preorderLabel ++ " ‹") ]
                ]
            ]
        )


title : String -> String
title lang =
    case lang of
        "fr" ->
            "Disques" |> Shared.titleWith

        "en" ->
            "Recordings" |> Shared.titleWith

        _ ->
            ""


bandcampPlayer : Int -> String -> String -> Html.Html msg
bandcampPlayer albumId link albumTitle =
    Html.iframe
        [ Html.Attributes.class "recording__player"
        , Html.Attributes.src <| "https://bandcamp.com/EmbeddedPlayer/album=" ++ String.fromInt albumId ++ "/size=small/bgcol=ffffff/linkcol=0687f5/artwork=none/transparent=true/"
        , Html.Attributes.attribute "seamless" ""
        ]
        [ Html.a
            [ Html.Attributes.href link ]
            [ Html.text <| albumTitle ++ " - Pig Society" ]
        ]
