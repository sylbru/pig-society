module Page.Lang_.Media exposing (Data, Model, Msg, page)

import Browser.Navigation
import DataSource exposing (DataSource)
import Dict exposing (Dict)
import Head
import Head.Seo as Seo
import Helpers
import Html exposing (Html)
import Html.Attributes as Attr
import Html.Events as Events
import LanguageTag
import LanguageTag.Language
import Page exposing (Page, PageWithState, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Path
import Quote
import Shared
import Video exposing (Video)
import View exposing (View)


type alias Model =
    { showMore : Bool
    , loadedVideos : List Video
    }


type Msg
    = ShowMore
    | ClickedVideo Video


type alias RouteParams =
    { lang : String }


page : PageWithState RouteParams Data Model Msg
page =
    Page.prerender
        { head = head
        , routes = routes
        , data = data
        }
        |> Page.buildWithLocalState { view = view, init = init, update = update, subscriptions = subscriptions }


routes : DataSource (List RouteParams)
routes =
    DataSource.succeed Helpers.langRoutes


data : RouteParams -> DataSource Data
data routeParams =
    DataSource.succeed ()


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    let
        language =
            (if static.routeParams.lang == "fr" then
                LanguageTag.Language.fr

             else
                LanguageTag.Language.en
            )
                |> LanguageTag.build LanguageTag.emptySubtags
                |> Head.rootLanguage

        texts =
            if static.routeParams.lang == "fr" then
                { description = "Vidéos et photos de la Pig Society"
                , alt = "Pig Society - High On A Mountain (Reims, 2019)"
                , title = "Média"
                }

            else
                { description = "Videos and photos of the Pig Society"
                , alt = "Pig Society - High On A Mountain (Reims, 2019)"
                , title = "Media"
                }

        seo =
            Seo.summary
                { canonicalUrlOverride = Nothing
                , siteName = Shared.siteName
                , image =
                    { url = Pages.Url.fromPath <| Path.fromString "/images/pigsociety-video-reims-vantor.jpg"
                    , alt = texts.alt
                    , dimensions = Just { width = 1440, height = 810 }
                    , mimeType = Just "image/jpeg"
                    }
                , description = texts.description
                , locale = Just static.routeParams.lang
                , title = texts.title
                }
                |> Seo.website
    in
    language :: seo


type alias Data =
    ()


view :
    Maybe PageUrl
    -> Shared.Model
    -> Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel localModel static =
    let
        lang =
            static.routeParams.lang

        text =
            if lang == "fr" then
                textFr

            else
                textEn
    in
    { title = text "title" |> Shared.titleWith
    , body =
        [ Quote.view lang Quote.Right Quote.petiteMusique
        , Html.h2 [] [ Html.text <| text "videos" ]
        , Video.toIframe Video.burningBushHalfPastFour localModel.loadedVideos ClickedVideo
        , Video.toIframe Video.muziekPublique localModel.loadedVideos ClickedVideo
        , Quote.view lang Quote.Left Quote.vite
        , Video.toIframe Video.noneButOne localModel.loadedVideos ClickedVideo
        , Video.toIframe Video.reimsVanTor localModel.loadedVideos ClickedVideo
        , Quote.view lang Quote.Right Quote.pasMal
        , Video.toIframe Video.cdfSession localModel.loadedVideos ClickedVideo
        , Video.toIframe Video.bowlingGreen localModel.loadedVideos ClickedVideo
        ]
            ++ (if localModel.showMore then
                    List.map (\video -> Video.toIframe video localModel.loadedVideos ClickedVideo) Video.more

                else
                    [ Html.p
                      []
                      [ Html.button [ Events.onClick ShowMore, Attr.class "button" ] [ Html.text (text "see_more") ] ]
                    ]
               )
    }


init : Maybe PageUrl -> Shared.Model -> StaticPayload Data RouteParams -> ( Model, Cmd Msg )
init _ _ _ =
    ( { showMore = False
      , loadedVideos = []
      }
    , Cmd.none
    )


update : PageUrl -> Maybe Browser.Navigation.Key -> Shared.Model -> StaticPayload Data RouteParams -> Msg -> Model -> ( Model, Cmd Msg )
update _ _ _ _ msg model =
    case msg of
        ShowMore ->
            ( { model | showMore = True }, Cmd.none )

        ClickedVideo video ->
            ( { model
                | loadedVideos =
                    if not <| List.member video model.loadedVideos then
                        video :: model.loadedVideos

                    else
                        model.loadedVideos
              }
            , Cmd.none
            )


subscriptions : Maybe PageUrl -> RouteParams -> Path.Path -> Model -> Sub Msg
subscriptions _ _ _ _ =
    Sub.none


textFr : String -> String
textFr id =
    Dict.get id textsFr |> Maybe.withDefault ""


textEn : String -> String
textEn id =
    Dict.get id textsEn |> Maybe.withDefault ""


textsFr : Dict String String
textsFr =
    Dict.fromList
        [ ( "title", "Média" )
        , ( "photos", "Photos" )
        , ( "videos", "Vidéos" )
        , ( "see_more", "Voir plus" )
        ]


textsEn : Dict String String
textsEn =
    Dict.fromList
        [ ( "title", "Media" )
        , ( "photos", "Photos" )
        , ( "videos", "Videos" )
        , ( "see_more", "See more" )
        ]
