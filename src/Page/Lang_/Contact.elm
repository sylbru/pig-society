module Page.Lang_.Contact exposing (Data, Model, Msg, page)

import DataSource exposing (DataSource)
import Head
import Head.Seo as Seo
import Helpers
import Html
import Html.Attributes as Attr
import LanguageTag
import LanguageTag.Language
import Page exposing (Page, PageWithState, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Path
import Quote
import Shared
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    { lang : String }


page : Page RouteParams Data
page =
    Page.prerender
        { head = head
        , routes = routes
        , data = data
        }
        |> Page.buildNoState { view = view }


routes : DataSource (List RouteParams)
routes =
    DataSource.succeed Helpers.langRoutes


data : RouteParams -> DataSource Data
data routeParams =
    DataSource.succeed ()


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    let
        language =
            (if static.routeParams.lang == "fr" then
                LanguageTag.Language.fr

             else
                LanguageTag.Language.en
            )
                |> LanguageTag.build LanguageTag.emptySubtags
                |> Head.rootLanguage

        texts =
            if static.routeParams.lang == "fr" then
                { description = "Informations de contact pour Pig Society."
                , alt = "Contacter Pig Society"
                , title = "Contact"
                }

            else
                { description = "Contact info for the Pig Society"
                , alt = "Contact Pig Society"
                , title = "Contact"
                }

        seo =
            Seo.summary
                { canonicalUrlOverride = Nothing
                , siteName = Shared.siteName
                , image =
                    { url = Pages.Url.fromPath <| Path.fromString "/images/contact.jpg"
                    , alt = texts.alt
                    , dimensions = Just { width = 640, height = 427 }
                    , mimeType = Just "image/jpeg"
                    }
                , description = texts.description
                , locale = Just static.routeParams.lang
                , title = texts.title
                }
                |> Seo.website
    in
    language :: seo


type alias Data =
    ()


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    { title = Shared.titleWith "Contact"
    , body =
        [ Quote.view static.routeParams.lang Quote.Left Quote.bouffe
        , Html.p []
            [ Html.a [ Attr.href "mailto:contact@pigsociety.eu" ] [ Html.text "contact@pigsociety.eu" ]
            ]
        , Html.p []
            [ Html.a [ Attr.href "https://www.facebook.com/pigssociety" ] [ Html.text "Facebook" ]
            ]
        , Html.p []
            [ Html.a [ Attr.href "https://www.instagram.com/pigsociety.band/" ] [ Html.text "Instagram" ]
            ]
        , Quote.view static.routeParams.lang Quote.Right Quote.tout
        ]
    }
