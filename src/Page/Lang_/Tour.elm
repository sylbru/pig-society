module Page.Lang_.Tour exposing (Data, Model, Msg, page)

import Browser.Navigation
import DataSource exposing (DataSource)
import DataSource.File
import Date
import Head
import Head.Seo as Seo
import Helpers exposing (MultilingualText)
import Html
import Html.Attributes exposing (class)
import LanguageTag
import LanguageTag.Language
import Page exposing (Page, PageWithState, StaticPayload)
import Pages
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Path
import Quote
import Shared
import Task
import Time
import TourDate exposing (TourDate)
import View exposing (View)
import Yaml.Decode


{-| Data sorted frontend-side to make sur it’s up-to-date with regard to the future/past sections
-}
type alias Model =
    Maybe Data


type Msg
    = GotTime Time.Posix


type alias RouteParams =
    { lang : String }


{-| Contains the list of tour dates separated in future/past dates
using the build date (will be sorted again in the model)
-}
type alias Data =
    { future : List TourDate
    , past : List TourDate
    }


page : PageWithState RouteParams Data Model Msg
page =
    Page.prerender
        { head = head
        , routes = routes
        , data = data
        }
        |> Page.buildWithLocalState
            { view = view
            , init = init
            , update = update
            , subscriptions = \_ _ _ _ -> Sub.none
            }


routes : DataSource (List RouteParams)
routes =
    DataSource.succeed Helpers.langRoutes


data : RouteParams -> DataSource Data
data routeParams =
    let
        today =
            Date.fromPosix Time.utc Pages.builtAt

        tourDatesAsResults : DataSource (List (Result String TourDate))
        tourDatesAsResults =
            DataSource.File.rawFile "content/tour_dates.yaml"
                |> DataSource.andThen
                    (\tourDatesYaml ->
                        case Yaml.Decode.fromString (Yaml.Decode.list TourDate.decoder) tourDatesYaml of
                            Ok result ->
                                DataSource.succeed result

                            Err _ ->
                                DataSource.fail <| "Couldn’t correctly read tour dates from content/tour_dates.yaml: \n" ++ tourDatesYaml
                    )

        tourDates : DataSource (List TourDate)
        tourDates =
            DataSource.andThen
                (\listResults ->
                    let
                        listDates =
                            List.filterMap Result.toMaybe listResults
                    in
                    if List.length listResults /= List.length listDates then
                        let
                            errors =
                                List.filterMap toMaybeError listResults
                        in
                        DataSource.fail ("Couldn’t parse a date in content/tour_dates.yml: \n" ++ String.join "\n" errors)

                    else
                        DataSource.succeed listDates
                )
                tourDatesAsResults
    in
    DataSource.map
        (splitPastFutureDates today)
        tourDates
        |> DataSource.map
            (\( future, past ) ->
                { future = sortDatesEarliestFirst future
                , past = sortDatesMostRecentFirst past
                }
            )


splitPastFutureDates : Date.Date -> List TourDate -> ( List TourDate, List TourDate )
splitPastFutureDates today =
    List.partition
        (\tourDate ->
            Date.compare tourDate.date today
                /= LT
        )


sortDatesEarliestFirst : List TourDate -> List TourDate
sortDatesEarliestFirst =
    List.sortWith (\tourDateA tourDateB -> Date.compare tourDateA.date tourDateB.date)


sortDatesMostRecentFirst : List TourDate -> List TourDate
sortDatesMostRecentFirst =
    sortDatesEarliestFirst >> List.reverse


init : Maybe PageUrl -> Shared.Model -> StaticPayload Data RouteParams -> ( Model, Cmd Msg )
init _ _ _ =
    ( Nothing, Task.perform GotTime Time.now )


update : PageUrl -> Maybe Browser.Navigation.Key -> Shared.Model -> StaticPayload Data RouteParams -> Msg -> Model -> ( Model, Cmd Msg )
update _ _ _ static msg model =
    case msg of
        GotTime now ->
            ( reSortDates now static model, Cmd.none )


reSortDates : Time.Posix -> StaticPayload Data RouteParams -> Model -> Model
reSortDates now static model =
    let
        today =
            Date.fromPosix Time.utc now

        allDates =
            static.data.future ++ static.data.past

        ( futureDates, pastDates ) =
            splitPastFutureDates today allDates
    in
    Just <|
        { future = sortDatesEarliestFirst futureDates
        , past = sortDatesMostRecentFirst pastDates
        }


toMaybeError : Result x a -> Maybe x
toMaybeError result =
    case result of
        Ok _ ->
            Nothing

        Err error ->
            Just error


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    let
        language =
            (if static.routeParams.lang == "fr" then
                LanguageTag.Language.fr

             else
                LanguageTag.Language.en
            )
                |> LanguageTag.build LanguageTag.emptySubtags
                |> Head.rootLanguage

        texts =
            if static.routeParams.lang == "fr" then
                { description = "Dates de tournée de la Pig Society"
                , alt = "Pig Society au Celebration Days Festival en 2018"
                , title = "Dates"
                }

            else
                { description = "Tour dates for the Pig Society"
                , alt = "Pig Society at the Celebration Days Festival in 2018"
                , title = "Tour"
                }

        seo =
            Seo.summary
                { canonicalUrlOverride = Nothing
                , siteName = Shared.siteName
                , image =
                    { url = Pages.Url.fromPath <| Path.fromString "/images/pigsociety-tour-cdf.jpg"
                    , alt = texts.alt
                    , dimensions = Just { width = 641, height = 641 }
                    , mimeType = Just "image/jpeg"
                    }
                , description = texts.description
                , locale = Just static.routeParams.lang
                , title = texts.title
                }
                |> Seo.website
    in
    language :: seo


view :
    Maybe PageUrl
    -> Shared.Model
    -> Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel model static =
    let
        lang =
            static.routeParams.lang

        toLang =
            if lang == "fr" then
                .fr

            else
                .en

        ( futureDates, pastDates ) =
            case model of
                Just resortedData ->
                    ( resortedData.future, resortedData.past )

                Nothing ->
                    ( static.data.future, static.data.past )
    in
    { title = text.title |> toLang
    , body =
        [ Quote.view lang Quote.Left Quote.voyage
        , Html.h2 [] [ Html.text (text.future |> toLang) ]
        , Html.ul [ class "tour" ] (List.map (TourDate.view lang) futureDates)
        , Quote.view lang Quote.Right Quote.tempsMort
        , Html.h2 [] [ Html.text (text.past |> toLang) ]
        , Html.ul [ class "tour" ] (List.map (TourDate.view lang) pastDates)
        ]
    }


text : { title : MultilingualText, future : MultilingualText, past : MultilingualText }
text =
    { title = { fr = Shared.titleWith "Dates", en = Shared.titleWith "Tour" }
    , future = { fr = "C’est à venir\u{202F}!", en = "Coming soon!" }
    , past = { fr = "(C’est du passé)", en = "Good old times…" }
    }
